import axios from "./axios";

export const SetSession = (_token) => {
  if (_token) {
    localStorage.setItem("token", _token);
    axios.defaults.headers.common["TRIPX-User-Token"] = `${_token}`;
    axios.defaults.headers.common["Content-Type"] = "application/json";
  } else {
    localStorage.removeItem("token");

    delete axios.defaults.headers.common.Authorization;
  }
};
