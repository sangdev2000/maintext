export const formatNumber = (num) => {
  if (num) {
    let number = Number(num);
    return new Intl.NumberFormat("en-US").format(number);
  } else {
    return "0";
  }
};
