import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "https://api-init-kvep.onrender.com/",
});
// export const axiosInstanceDumy = axios.create({
//   baseURL: "https://dummyjson.com/",
// });
// axiosInstanceDumy.interceptors.response.use(
//   (response) => response.data,
//   (error) =>
//     Promise.reject(
//       (error.response && error.response.data) || "ERROR MASAESE"
//     )
// );
axiosInstance.interceptors.response.use(
  (response) => response.data,
  (error) =>
    Promise.reject(
      (error.response && error.response.data) || "Something went wrong"
    )
);

export default axiosInstance;
const token = localStorage.getItem("tokent");
export const instanceAuth = axios.create({
  // baseURL: process.env.REACT_APP_API_URL,
  baseURL: "https://api-init-kvep.onrender.com/",
  headers: {
    Accept: "application/json",
    token: `Bearer ${token}`,
  },
});
// export const instanceAuthDUmy = axios.create({
//   // baseURL: process.env.REACT_APP_API_URL,
//   baseURL: "https://dummyjson.com/",
//   headers: {
//     Accept: "application/json",
//     token: `Bearer ${token}`,
//   },
// });
