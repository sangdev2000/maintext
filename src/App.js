import React, { useState } from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./common/header/Header";
import Landing from "./pages/Landing";
import Data from "./components/Data";
import Cart from "./common/Cart/Cart";
import Footer from "./common/footer/Footer";
import Sdata from "./components/shops/Sdata";
import Login from "./pages/Login/login";
import CreateProduct from "./pages/createProduct";
import ProductCms from "./pages/ProductCms";
import Detail from "./pages/detail";
import UpdataProdux from "./components/updataProdux";
import ListAllUser from "./components/getAlluser";
import DetailsUser from "./components/detailsuser";
import UpdateUser from "./components/updateuser";
import CreatOder from "./components/creatoder";
import RegisterLogin from "./components/Register";
import MyduApi from "./components/apiDummy";
function App() {
  return (
    <>
      <Router>
        <Header />
        <Switch>
          <Route path="/" exact>
            <Landing />
          </Route>
          <Route path="/register" exact>
            <RegisterLogin />
          </Route>
          <Route path="/login" exact>
            <Login />
          </Route>
          <Route path="/cart" exact>
            <Cart />
          </Route>
          <Route path="/create-product" exact>
            <CreateProduct />
          </Route>
          <Route path="/admin-product" exact>
            <ProductCms />
          </Route>
          <Route path="/detail/:id" exact>
            <Detail />
          </Route>
          <Route path="/updata/:id" exact>
            <UpdataProdux />
          </Route>
          <Route path="/list-user" exact>
            <ListAllUser />
          </Route>
          <Route path="/details-user/:id" exact>
            <DetailsUser />
          </Route>
          <Route path="/uptate_user/:id" exact>
            <UpdateUser />
          </Route>
          <Route path="/creatoder/:id" exact>
            <CreatOder />
          </Route>
          <Route path="/dumy" exact>
            <MyduApi />
          </Route>
        </Switch>
        <Footer />
      </Router>
    </>
  );
}

export default App;
