import { useState } from "react";
import "./style.css";
import axiosInstance from "../../utils/axios";
import { Link, useHistory } from "react-router-dom/cjs/react-router-dom";

const RegisterLogin = () => {
  const history = useHistory();
  const [regisTer, setregisTer] = useState({
    name: "",
    email: "",
    password: "",
    confirmPassword: "",
    phone: 0,
  });
  const handleregister = async () => {
    if (
      !regisTer.name ||
      !regisTer.email ||
      !regisTer.password ||
      !regisTer.confirmPassword ||
      !regisTer.phone
    ) {
      return;
    }
    const res = await axiosInstance.post("api/user/sign-up", regisTer);
    console.log(res);
    if (res.status === "OK") {
      history.push("/login");
    }
  };
  return (
    <form autocomplete="off" class="form">
      <div class="control block-cube block-input">
        <h1>Sign In</h1>
      </div>
      <div class="control block-cube block-input">
        <input
          name="name"
          placeholder="name"
          value={regisTer.name}
          type="text"
          onChange={(e) => {
            setregisTer({ ...regisTer, name: e.target.value });
          }}
        />
        <div class="bg-top">
          <div class="bg-inner"></div>
        </div>
        <div class="bg-right">
          <div class="bg-inner"></div>
        </div>
        <div class="bg">
          <div class="bg-inner"></div>
        </div>
      </div>
      <div class="control block-cube block-input">
        <input
          name="email"
          placeholder="email"
          value={regisTer.email}
          type="text"
          onChange={(e) => {
            setregisTer({ ...regisTer, email: e.target.value });
          }}
        />
        <div class="bg-top">
          <div class="bg-inner"></div>
        </div>
        <div class="bg-right">
          <div class="bg-inner"></div>
        </div>
        <div class="bg">
          <div class="bg-inner"></div>
        </div>
      </div>
      <div class="control block-cube block-input">
        <input
          name="password"
          placeholder="password"
          value={regisTer.password}
          type="text"
          onChange={(e) => {
            setregisTer({ ...regisTer, password: e.target.value });
          }}
        />
        <div class="bg-top">
          <div class="bg-inner"></div>
        </div>
        <div class="bg-right">
          <div class="bg-inner"></div>
        </div>
        <div class="bg">
          <div class="bg-inner"></div>
        </div>
      </div>
      <div class="control block-cube block-input">
        <input
          name="confirmPassword"
          placeholder="confirmPassword"
          value={regisTer.confirmPassword}
          type="text"
          onChange={(e) => {
            setregisTer({ ...regisTer, confirmPassword: e.target.value });
          }}
        />
        <div class="bg-top">
          <div class="bg-inner"></div>
        </div>
        <div class="bg-right">
          <div class="bg-inner"></div>
        </div>
        <div class="bg">
          <div class="bg-inner"></div>
        </div>
      </div>
      <div class="control block-cube block-input">
        <input
          name="phone"
          placeholder="phone"
          value={regisTer.phone}
          type="text"
          onChange={(e) => {
            setregisTer({ ...regisTer, phone: e.target.value });
          }}
        />
        <div class="bg-top">
          <div class="bg-inner"></div>
        </div>
        <div class="bg-right">
          <div class="bg-inner"></div>
        </div>
        <div class="bg">
          <div class="bg-inner"></div>
        </div>
      </div>
      <button class="btn block-cube block-cube-hover" type="button">
        <div class="bg-top">
          <div class="bg-inner"></div>
        </div>
        <div class="bg-right">
          <div class="bg-inner"></div>
        </div>
        <div class="bg">
          <div class="bg-inner"></div>
        </div>
        <div class="text" onClick={handleregister}>
          regisTer
        </div>
      </button>
      <div class="credits">
        <a href="https://codepen.io/marko-zub/" target="_blank">
          My other codepens
        </a>
      </div>
      <div style={{ paddingTop: "20px" }}>
        <Link to={"/login"}>dang nhap?</Link>
      </div>
    </form>
  );
};
export default RegisterLogin;
