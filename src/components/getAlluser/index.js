import React, { useEffect, useState } from "react";
import axios from "axios";
import ENDPOINT from "../../services/Endpoint";
import { instanceAuth } from "../../utils/axios";
import "./style.css";
import { Link } from "react-router-dom/cjs/react-router-dom";
const ListAllUser = () => {
  const [users, setUsers] = useState([]);
  const handleListuser = async () => {
    try {
      const response = await instanceAuth.get(ENDPOINT.GETALLUSER);
      if (response?.data !== "ERR") {
        setUsers(response?.data?.data);
        console.log("i_d", response);
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };
  useEffect(() => {
    handleListuser();
  }, []);
  console.log("name", users);
  return (
    <div class="content">
      {users?.map((item, index) => {
        return (
          <ul class="team">
            <li class="member co-funder">
              <div
                class="thumb"
                style={{ color: "#fff", fontSize: "16px", textAlign: "center" }}
              >
                <h1>{item.name}</h1>
              </div>
              <div class="description" style={{ fontSize: "12px" }}>
                <h1>{item.email}</h1>
                <h1>{item.password}</h1>
              </div>
            </li>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                gap: "100px", 
              }}
            >                        
              <Link to={`/details-user/${item._id}`} class="thumb">
                xem chi tiet
              </Link>
              <Link to={`/uptate_user/${item._id}`} class="thumb">
                UPTATE USER
              </Link>
              <button
                class="thumb"
                onClick={async () => {
                  try {
                    const response = await instanceAuth.delete(
                      `api/user/delete-user/${item._id}`
                    );
                    if (response?.data !== "ERR") {
                      console.log("delete", users);
                      //   handleupdateuser(param);
                      handleListuser();
                    }
                  } catch (error) {
                    console.error("Error:", error);
                  }
                }}
              >
                DELETE
              </button>
            </div>
          </ul>
        );
      })}
    </div>
  );
};
export default ListAllUser;
