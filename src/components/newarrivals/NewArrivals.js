import React, { useEffect, useState } from "react";
import Cart from "./Cart";
import "./style.css";
import { getAllProduct } from "../../redux/slices/Product";
import { useDispatch, useSelector, RootState } from "../../redux/store";
import { getAllProductDumy } from "../../redux/slices/ProductDumy";
const NewArrivals = () => {
  const dispatch = useDispatch();
  const [param, setParam] = useState({
    limit: 30,
    page: 1,
  });
  // const ProductListdumy = useSelector((state) => state.Productdumy.ProductListdumy.data);
  // console.log(ProductListdumy);
  const Product = useSelector((state) => state.product.ProductList.data);
  console.log("papa", Product);
  useEffect(() => {
    dispatch(getAllProduct(param));
  }, [param]);
  return (
    <>
      <section className="NewArrivals background">
        <div className="container">
          <div className="heading d_flex">
            <div className="heading-left row  f_flex">
              <img src="https://img.icons8.com/glyph-neue/64/26e07f/new.png" />
              <h2>New Arrivals</h2>
            </div>
            <div className="heading-right row">
              <span>View all</span>
              <i className="fa-solid fa-caret-right"></i>
            </div>
          </div>
          <Cart data={Product} />
        </div>
      </section>
    </>
  );
};
export default NewArrivals;
