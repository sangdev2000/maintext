import React, { useState } from "react";
import { Link } from "react-router-dom/cjs/react-router-dom";
import styled from "styled-components";
import cart from "../../acset/img/Cart.png";
const Cart = ({ data }) => {
  const [ime, setime] = useState([]);
  console.log("xiaxia", data);
  return (
    <>
      <WapperAll>
        {data &&
          data.map((val, index) => {
            return (
              <WapperContainerr>
                <Link
                  to={`/detail/${val._id}`}
                  className="box"
                  key={val._id}
                  style={{ alignSelf: "stretch" }}
                >
                  <div style={{ alignSelf: "stretch" }}>
                    <img
                      style={{
                        width: "100%",
                        height: "100px",
                        borderRadius: "20px",
                      }}
                      src={val.image[0]}
                      alt=""
                    />
                  </div>
                  <h4>{val.name}</h4>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      gap: "10px",
                    }}
                  >
                    <span>${val.price}</span>
                    <span>them vao don hang</span>
                  </div>

                  <Link to={`/creatoder/${val._id}`}>
                    <img
                      src={cart}
                      style={{
                        width: "10%",
                        height: "10%",
                        position: "absolute",
                        right: "0",
                        top: "270px",
                      }}
                    ></img>
                  </Link>
                </Link>
              </WapperContainerr>
            );
          })}
      </WapperAll>
    </>
  );
};
export default Cart;
export const WapperContainerr = styled.div`
  width: calc((100%) / 4);
  height: 350px;
  align-items: center;
  align-self: stretch;
  background: #ccc;
  padding: 20px 20px;
  position: relative;
  @media only screen and (max-width: 700px) {
    width: calc((100%) / 2);
  }
`;
export const WapperAll = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  background: #000;
  justify-content: space-between;
  margin: 10px 10px;
  border-radius: 40px;
  align-self: stretch;
  gap: 10px;
`;
