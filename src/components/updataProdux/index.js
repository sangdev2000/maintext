import { useParams } from "react-router-dom/cjs/react-router-dom";
import { useEffect, useState } from "react";
import "./style.css";
import { instanceAuth } from "../../utils/axios";
import { dispatch } from "../../redux/store";
const UpdataProdux = () => {
  const { id } = useParams();
  const [param, setParam] = useState({});
  const handleGetDetail = async () => {
    await instanceAuth
      .get(`https://api-init-kvep.onrender.com/api/product/get-details/${id}`)
      .then((res) => {
        console.log(res);
        setParam(res?.data?.data);
      });
  };
  useEffect(() => {
    handleGetDetail();
  }, []);
  console.log(param);
  return (
    <div className="app">
      <div className="details">
        <div className="big-img">
          <input
            className="big-img"
            type="text"
            placeholder="ảnh..."
            onChange={(e) => {
              setParam({ ...param, image: e.target.value });
            }}
          />
        </div>

        <div className="box">
          <div className="row">
            <input
              type="text"
              value={param.name}
              onChange={(e) => {
                setParam({ ...param, name: e.target.value });
              }}
            />
            <input
              type="text"
              value={param.price}
              onChange={(e) => {
                setParam({ ...param, price: e.target.value });
              }}
            />
          </div>
          <div className="row">
            <input
              type="text"
              value={param.rating}
              onChange={(e) => {
                setParam({ ...param, rating: e.target.value });
              }}
            />
            <input
              type="text"
              value={param.discount}
              onChange={(e) => {
                setParam({ ...param, discount: e.target.value });
              }}
            />
            <input
              type="text"
              value={param.type}
              onChange={(e) => {
                setParam({ ...param, type: e.target.value });
              }}
            />
          </div>
          <div className="row">
            <input
              type="text"
              value={param.updatedAt}
              onChange={(e) => {
                setParam({ ...param, updatedAt: e.target.value });
              }}
            />
          </div>
          <div className="colors"></div>

          <input
            type="text"
            value={param.description}
            onChange={(e) => {
              setParam({ ...param, description: e.target.value });
            }}
          />

          {/* <DetailsThumb images={data.src} tab={this.handleTab} myRef={this.myRef} /> */}
          <button
            className="cart"
            onClick={async () => {
              try {
                const response = await instanceAuth.put(
                  `https://api-init-kvep.onrender.com/api/product/update/${id}`,
                  param
                );
                handleGetDetail(param);
              } catch (error) {
                console.error("Error:", error);
              }
            }}
          >
            Updata
          </button>
        </div>
      </div>
    </div>
  );
};
export default UpdataProdux;

let colors = ["red", "black", "crimson", "teal"];
