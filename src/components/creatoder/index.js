import { useEffect, useState } from "react";
import "./style.css";
import { instanceAuth } from "../../utils/axios";
import { useDispatch, useSelector } from "../../redux/store";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import Login from "../../pages/Login/login";
const CreatOder = () => {
  const { id } = useParams();
  const [data, setdata] = useState({});
  const [user, setuser] = useState({});
  const [order, setOrder] = useState({
    orderItems: [
      {
        product: `${id}`,
        amount: 0,
      },
    ],
    paymentMethod: "",
    fullName: "",
    address: "",
    city: "",
    phone: "",
    user: "",
  });
  useEffect(() => {
    const userid = localStorage.getItem("userId");
    setOrder((prevOrder) => ({ ...prevOrder, user: userid }));
    console.log("userid", order);
  }, []);
  useEffect(async () => {
    const handleGetDetailOder = async () => {
      await instanceAuth
        .get(`https://api-init-kvep.onrender.com/api/product/get-details/${id}`)
        .then((res) => {
          console.log(res);
          setdata(res?.data?.data);
          console.log("sada", data);
        });
    };
    handleGetDetailOder();
  }, []);
  const handlepost = async (event) => {
    event.preventDefault();
    try {
      if (
        !order.address ||
        !order.city ||
        !order.fullName ||
        !order.paymentMethod ||
        !order.phone ||
        !order.user
      ) {
        return;
      }
      await instanceAuth.post("/api/order/create", order);
      alert("tạo đơn thành công");
    } catch (error) {
      console.error("Error creating order:", error);
    }
  };
  console.log("param", order);
  return (
    <div class="overlay">
      <form>
        <div class="con">
          <header class="head-form">
            <h2>OderProduxct</h2>
          </header>
          <br />
          <div class="con">
            <input
              value={order.orderItems[0].amount}
              placeholder="amount"
              onChange={(e) => {
                setOrder({
                  ...order,
                  orderItems: [
                    {
                      product: `${id}`,
                      amount: e.target.value,
                    },
                  ],
                });
              }}
            />
          </div>
          <div class="con">
            <input
              value={order.paymentMethod}
              placeholder="paymentMethod"
              onChange={(e) => {
                setOrder({ ...order, paymentMethod: e.target.value });
              }}
            />
          </div>
          <div class="con">
            <input
              value={order.fullName}
              placeholder="fullName"
              onChange={(e) => {
                setOrder({ ...order, fullName: e.target.value });
              }}
            />
          </div>
          <div class="con">
            <input
              value={order.address}
              placeholder="address"
              onChange={(e) => {
                setOrder({ ...order, address: e.target.value });
              }}
            />
          </div>
          <div class="con">
            <input
              value={order.city}
              placeholder="city"
              onChange={(e) => {
                setOrder({ ...order, city: e.target.value });
              }}
            />
          </div>
          <div class="con">
            <input
              value={order.phone}
              placeholder="phone"
              onChange={(e) => {
                setOrder({ ...order, phone: e.target.value });
              }}
            />
          </div>
          <div class="con">
            <input
              value={order.orderItems[0].product}
              placeholder="product"
              onChange={(e) => {
                setOrder({
                  ...order,
                  orderItems: [
                    {
                      ...order.orderItems[0],
                      product: e.target.value,
                    },
                  ],
                });
              }}
            />
          </div>
          <button class="button" onClick={handlepost}>
            ODER
          </button>
        </div>
      </form>
    </div>
  );
};
export default CreatOder;
