import { useState } from "react";

const AmanitionButton = () => {
  const [isHovered, setHovered] = useState(false);
  return (
    <>
      <svg>
        <image
          filter="url(#blur)"
          onMouseEnter={() => setHovered(true)}
          onMouseLeave={() => setHovered(false)}
        />
        <filter id="blur">
          <motion.div
            initial={false}
            animate={{ stdDeviation: isHovered ? 0 : 2 }}
          />
        </filter>
      </svg>
      <motion.a
        whileHover={{ scale: 1.2 }}
        onHoverStart={(e) => {}}
        onHoverEnd={(e) => {}}
      />
    </>
  );
};
export default AmanitionButton;
