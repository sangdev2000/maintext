import { useEffect, useState } from "react";
import { instanceAuth } from "../../utils/axios";
import { useParams } from "react-router-dom/cjs/react-router-dom";
const DetailsUser = () => {
  const { id } = useParams();
  const [listuser, setlistuser] = useState([]);
  useEffect(() => {
    const handlelistdetails = async () => {
      try {
        await instanceAuth.get(`/api/user/get-details/${id}`).then((res) => {
          console.log(res);
          setlistuser(res?.data?.data);
        });
      } catch (error) {
        console.error("Error:", error);
      }
    };
    handlelistdetails();
  }, []);
  return (
    <>
      <div
        className="app"
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          textAlign: "left",
        }}
      >
        <div className="details" key={listuser._id}>
          <div className="box">
            <div className="row">
              <h2>{listuser.name}</h2>
            </div>
            <div className="row">
              <span>${listuser.phone}</span>
            </div>
            <div className="row">
              <h2>{listuser.email}</h2>
            </div>
            <div className="row">
              <h2>{listuser.updatedAt}</h2>
            </div>
            <div className="row">
              <h2>{listuser.password}</h2>
            </div>
            <div className="row">
              <h2>{listuser.createdAt}</h2>
            </div>
            <div className="colors"></div>
            <button className="cart">Add to cart</button>
          </div>
        </div>
      </div>
    </>
  );
};
export default DetailsUser;
