import styled from "styled-components";

const Table = ({ data, keyObj }) => {
  return (
    <TableContainer>
      <table>
        <thead>
          <tr>
            {keyObj &&
              keyObj.map((item, index) => {
                return <th key={`${item.th}${index}`}>{item.th}</th>;
              })}
          </tr>
        </thead>
        <tbody>
          {data &&
            keyObj &&
            data.map((_item, index) => (
              <tr key={`tr${index}`}>
                {keyObj?.map((key, _index) => (
                  <td key={`td${index}${_index}`}>
                    {key.callback ? key.callback(_item) : _item[key.key]}
                  </td>
                ))}
              </tr>
            ))}
        </tbody>
      </table>
    </TableContainer>
  );
};
export default Table;
export const TableContainer = styled.div`
  width: 100%;
  border-radius: 8px;
  border: 1px solid #3a3a3a;
  background: #28282a;
  padding: 0px 14px;
  overflow-x: auto;
  table {
    width: 100%;
    overflow: hidden;
    th,
    td {
      padding: 15px;
    }
    th {
      text-align: left;
      color: rgba(226, 232, 255, 0.7);
      font-size: 14px;
      font-weight: 300;
      line-height: 110%; /* 15.4px */
    }
    td {
      color: #fff;
      font-family: PoppinsMedium;
      font-size: 14px;
      font-weight: 500;
      line-height: 110%; /* 15.4px */
      letter-spacing: 0.14px;
    }
    tr {
      th,
      td {
        &:last-child {
          text-align: center;
        }
      }
    }
  }
`;
