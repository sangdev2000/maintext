import { useEffect, useRef, useState } from "react";
import styled from "styled-components";

const TableMobile = ({ data, keyList, collaps }: any) => {
  return (
    <TableMobileWapper>
      {data &&
        keyList &&
        data.map((item: any, index: number) => {
          return (
            <CartCommon
              item={item}
              keyList={keyList}
              collaps={collaps}
              key={`cart${index}`}
            />
          );
        })}
    </TableMobileWapper>
  );
};
export default TableMobile;
const CartCommon = ({ item, keyList, collaps }) => {
  const [isShow, setIsShow] = useState(true);
  const [height, setHeight] = useState<any>("fit-content");
  const elementRef = useRef(null);

  useEffect(() => {
    if (elementRef.current) {
      const { height } = elementRef.current.getBoundingClientRect();
      setHeight(height);
    }
  }, []);
  return (
    <CardBox>
      <CartTitles
        onClick={() => {
          collaps && setIsShow(!isShow);
        }}
      >
        {keyList?.title?.map((_item: any, _index: number) => (
          <CartTitlesBox>
            <h3>{_item.callback ? _item.callback(item) : item[_item.value]}</h3>
            {_item.name && <span>{_item.name}</span>}
            {collaps && keyList?.title?.length - 1 === _index && (
              <button>
                <img src="./../images/staking/arrow-square-down.svg" alt=""/>
              </button>
            )}
          </CartTitlesBox>
        ))}
      </CartTitles>
      <CartBody isshow={isShow} ref={elementRef} height={height}>
        {keyList?.body?.map((i_Body: any, i_index: number) => (
          <CartBodyRow key={`${i_Body.name}${i_index}`}>
            <CartBodyRowName>{i_Body.name}</CartBodyRowName>
            {i_Body.callback ? (
              i_Body.callback(item)
            ) : (
              <CartBodyRowValue>{item[i_Body.value]}</CartBodyRowValue>
            )}
          </CartBodyRow>
        ))}
      </CartBody>
    </CardBox>
  );
};

export const TableMobileWapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  gap: 8px;
  width: 100%;
`;
const CardBox = styled.div`
  padding: 12px;
  width: 100%;
  border-radius: 12px;
  background: #282828;
`;
const CartTitles = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 7px;
  border-bottom: 1px solid rgba(226, 232, 255, 0.2);
  width: 100%;
`;
const CartTitlesBox = styled.div`
  display: flex;
  align-items: center;
  gap: 5px;
  h3 {
    color: #fff;
    font-family: PoppinsMedium;
    font-size: 14px;
    font-weight: 500;
    line-height: 110%; /* 15.4px */
    letter-spacing: 0.14px;
  }
  span {
    color: #a4a7bb;
    font-size: 12px;
    font-weight: 300;
    line-height: 110%; /* 13.2px */
  }
`;
const CartBody = styled.div<{ isshow?: any; height?: any }>`
  display: flex;
  flex-direction: column;
  gap: 7px;
  padding-top: ${({ isshow }) => (isshow ? "7" : "0")}px;
  width: 100%;
  height: ${({ isshow }) => (isshow ? "fit-content" : "0")}px;
  transition: height 0.3s linear;
  overflow: hidden;
`;
const CartBodyRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`;
const CartBodyRowName = styled.p`
  color: rgba(226, 232, 255, 0.7);
  font-size: 14px;
  font-weight: 300;
  line-height: 110%; /* 15.4px */
  width: 49%;
`;
const CartBodyRowValue = styled.p`
  color: #fff;
  font-size: 14px;
  font-weight: 300;
  line-height: 110%; /* 15.4px */
  width: 49%;
  text-align: right;
`;
