import { useEffect, useState } from "react";
import {
  useHistory,
  useParams,
} from "react-router-dom";
import { instanceAuth } from "../../utils/axios";

const UpdateUser = () => {
  const { id } = useParams();
  const [param, setparam] = useState({});
  const history = useHistory();
  const handleupdateuser = async () => {
    try {
      await instanceAuth.get(`/api/user/get-details/${id}`).then((res) => {
        console.log(res);
        setparam(res?.data?.data);
      });
    } catch (error) {
      console.error("Error:", error);
    }
  };
  useEffect(() => {
    handleupdateuser();
  }, []);
  console.log("updatauser", param);
  return (
    <>
      <div class="container">
        <div class="screen">
          <div class="screen__content">
            <form class="login">
              <div class="login__field">
                <i class="login__icon fas fa-user"></i>
                <input
                  type="text"
                  class="login__input"
                  value={param.name}
                  onChange={(e) => {
                    setparam({ ...param, name: e.target.value });
                  }}
                />
              </div>
              <div class="login__field">
                <i class="login__icon fas fa-lock"></i>
                <input
                  type="text"
                  class="login__input"
                  value={param.email}
                  onChange={(e) => {
                    setparam({ ...param, email: e.target.value });
                  }}
                />
              </div>
              <div class="login__field">
                <i class="login__icon fas fa-lock"></i>
                <input
                  type="text"
                  class="login__input"
                  value={param.password}
                  onChange={(e) => {
                    setparam({ ...param, password: e.target.value });
                  }}
                />
              </div>
              <div class="login__field">
                <i class="login__icon fas fa-lock"></i>
                <input
                  type="text"
                  class="login__input"
                  value={param.confirmPassword}
                  onChange={(e) => {
                    setparam({ ...param, confirmPassword: e.target.value });
                  }}
                />
              </div>
              <div class="login__field">
                <i class="login__icon fas fa-lock"></i>
                <input
                  type="text"
                  class="login__input"
                  value={param.phone}
                  onChange={(e) => {
                    setparam({ ...param, phone: e.target.value });
                  }}
                />
              </div>
            </form>
            <div class="social-login" style={{ paddingTop: "40px", cursor: "pointer"}}>
              <h3
                onClick={async () => {
                  console.log("param", param);
                  try {
                    const response = await instanceAuth.put(
                      `/api/user/update-user/${param._id}`,
                      param
                    );
                    if (response?.data !== "ERR") {
                      setparam(response?.data?.data);
                      history.push("/list-user");
                      console.log("updateuser", response);
                      //   handleupdateuser(param);
                     
                    }
                  } catch (error) {
                    console.error("Error:", error);
                  }
                }}
              >
                uptateuser
              </h3>
              <div class="social-icons">
                <a href="#" class="social-login__icon fab fa-instagram"></a>
                <a href="#" class="social-login__icon fab fa-facebook"></a>
                <a href="#" class="social-login__icon fab fa-twitter"></a>
              </div>
            </div>
          </div>
          <div class="screen__background">
            <span class="screen__background__shape screen__background__shape4"></span>
            <span class="screen__background__shape screen__background__shape3"></span>
            <span class="screen__background__shape screen__background__shape2"></span>
            <span class="screen__background__shape screen__background__shape1"></span>
          </div>
        </div>
      </div>
    </>
  );
};
export default UpdateUser;
