import "./style.css";
import styled from "styled-components";
const Bglogin = () => {
  return <Bayr></Bayr>;
};
export default Bglogin;
export const Bayr = styled.div`
  width: 225px;
  height: 300px;
  transition: all 0.25s linear;
  background-image: url(https://estelle.github.io/10/files/sprite.png);
  animation: gangham 4s steps(23, start) infinite,
    movearound 12s steps(69, end) infinite alternate 24ms;
  animation-direction: normal, alternate;
  @keyframes gangham {
    0% {
      background-position: 0 0;
    }
    100% {
      background-position: -5175px 0;
    }
  }
  @keyframes movearound {
    0% {
      transform: translatex(0);
    }
    100% {
      transform: translatex(1000px);
    }
  }
`;
