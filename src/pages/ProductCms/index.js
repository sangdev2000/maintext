import React, { useEffect, useState } from "react";
import { getAllProduct } from "../../redux/slices/Product";
import { useDispatch, useSelector, RootState } from "../../redux/store";
import Table from "../../components/table";
import axios from "axios";
import ENDPOINT from "../../services/Endpoint";
import axiosInstance, { instanceAuth } from "../../utils/axios";
import { Link } from "react-router-dom/cjs/react-router-dom";

//---------------------------------------------------------------------------------//
const ProductCms = () => {
  const dispatch = useDispatch({});
  const [param, setParam] = useState({
    limit: 10,
    page: 1,
  });
  const ProductList = useSelector((state) => state.product.ProductList.data);
  console.log("sasasa", ProductList);
  useEffect(() => {
    dispatch(getAllProduct(param));
  }, []);
  console.log("data", ProductList);
  return (
    <Table
      data={ProductList}
      keyObj={[
        { th: "Rank", key: "rating" },
        { th: "name", key: "name" },
        {
          th: "image",
          key: "image",
          callback: (data) => (
            <div style={{ alignItems: "center" }}>
              {data?.image?.map((ime) => (
                <img
                  src={ime}
                  style={{ maxWidth: "100px", maxHeight: "100px" }}
                />
              ))}
            </div>
          ),
        },
        {
          th: "type",
          key: "type",
        },
        {
          th: "price",
          key: "price",
        },
        {
          th: "discount",
          key: "discount",
        },
        {
          th: "description",
          key: "description",
        },
        {
          th: "count in stock",
          key: "countInStock",
        },
        {
          th: "createdAt",
          key: "createdAt",
        },
        {
          th: "updatedAt",
          key: "updatedAt",
        },
        {
          th: "__v",
          key: "updatedAt",
        },
        {
          th: "Action",
          key: "",
          callback: (data) => (
            <div style={{ display: "flex", gap: "10px" }}>
              <button
                onClick={async () => {
                  try {
                    const response = await instanceAuth.delete(
                      `https://api-init-kvep.onrender.com/api/product/delete/${data._id}`
                    );
                    dispatch(getAllProduct(param));
                  } catch (error) {
                    console.error("Error:", error);
                  }
                }}
              >
                X
              </button>
              <Link to={`/updata/${data._id}`}>updata</Link>
              <Link to={`/list-user`}>get listuser</Link>
            </div>
          ),
        },
      ]}
    />
  );
};
export default ProductCms;
