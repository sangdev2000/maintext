import { useState } from "react";
import "./style.css";
import { instanceAuth } from "../../utils/axios";
import ENDPOINT from "../../services/Endpoint";
import { useHistory, useParams } from "react-router-dom/cjs/react-router-dom.min";
const CreateProduct = () => {
  const {id} = useParams()
  const history = useHistory();
  const [param, setParam] = useState({
    name: "",
    image: "",
    type: "",
    countInStock: 0,
    price: 0,
    rating: 10,
    description: "",
    discount: 10,
  });
  const handleCreate = async () => {
    if (
      !param.name ||
      !param.image ||
      !param.price ||
      !param.type ||
      !param.countInStock ||
      !param.description ||
      !param.discount
    ) {
      return;
    }
    const res = await instanceAuth.post(ENDPOINT.CREATEPRODUCT, param);
    history.push("/");
    console.log(res);
  };
  return (
    <div class="product-card">
      <div class="badge">Hot</div>
      <div class="product-tumb">
        {param.image && <img src={param.image} alt="" />}
      </div>
      <div class="product-details">
        <input
          class="product-catagory"
          type="text"
          placeholder="name"
          onChange={(e) => {
            setParam({ ...param, name: e.target.value });
          }}
        />
        <textarea
          style={{ width: "100%" }}
          placeholder="description"
          value={param.description}
          onChange={(e) => {
            setParam({ ...param, description: e.target.value });
          }}
        ></textarea>
        <div class="product-bottom-details">
          <div>
            <input
              class="product-price"
              type="text"
              value={param.image}
              placeholder="image"
              onChange={(e) => {
                setParam({ ...param, image: e.target.value });
              }}
            />
          </div>
          <div>
            <input
              class="product-price"
              type="text"
              placeholder="countInStock"
              value={param.countInStock}
              onChange={(e) => {
                setParam({ ...param, countInStock: e.target.value });
              }}
            />
          </div>
          <div>
            <input
              class="product-price"
              type="text"
              placeholder="price..."
              value={param.price}
              onChange={(e) => {
                setParam({ ...param, price: e.target.value });
              }}
            />
          </div>
          <div>
            <input
              class="product-price"
              type="text"
              placeholder="discount..."
              value={param.discount}
              onChange={(e) => {
                setParam({ ...param, discount: e.target.value });
              }}
            />
          </div>
          <div>
            <input
              class="product-price"
              type="text"
              placeholder="type..."
              value={param.type}
              onChange={(e) => {
                setParam({ ...param, type: e.target.value });
              }}
            />
          </div>
          <div class="product-links">
            <a href="">
              <i class="fa fa-heart"></i>
            </a>
            <a href="">
              <i class="fa fa-shopping-cart"></i>
            </a>
          </div>
        </div>
      </div>
      <button
        style={{ width: "100%", height: "40px" }}
        onClick={() => {
          handleCreate();
        }}
      >
        Submit
      </button>
    </div>
  );
};
export default CreateProduct;

