import React, { useState } from "react";
import "./style.css";
import axiosInstance from "../../utils/axios";
import ENDPOINT from "../../services/Endpoint";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom/cjs/react-router-dom";
import styled from "styled-components";
import Bglogin from "../../components/bglogin";
const Login = () => {
  const history = useHistory();
  const [param, setParam] = useState({
    email: "",
    password: "",
  });
  const handleLogin = async () => {
    if (!param.email || !param.password) {
      return;
    }
    const res = await axiosInstance.post(ENDPOINT.LOGIN, param);
    console.log(res);
    localStorage.setItem("tokent", res.access_token);
    localStorage.setItem("userId", res.id);
    console.log("user", res);
    if (res.status === "OK") {
      history.push("/create-product");
    }
  };
  return (
    <div style={{ zIndex: "20" }}>
      <form autocomplete="off" class="form">
        <div class="control">
          <h1>Sign In</h1>
        </div>
        <div class="control block-cube block-input">
          <input
            name="email"
            placeholder="email"
            value={param.email}
            type="text"
            onChange={(e) => {
              setParam({ ...param, email: e.target.value });
            }}
          />
          <div class="bg-top">
            <div class="bg-inner"></div>
          </div>
          <div class="bg-right">
            <div class="bg-inner"></div>
          </div>
          <div class="bg">
            <div class="bg-inner"></div>
          </div>
        </div>
        <div class="control block-cube block-input">
          <input
            name="password"
            placeholder="password"
            value={param.password}
            type="text"
            onChange={(e) => {
              setParam({ ...param, password: e.target.value });
            }}
          />
          <div class="bg-top">
            <div class="bg-inner"></div>
          </div>
          <div class="bg-right">
            <div class="bg-inner"></div>
          </div>
          <div class="bg">
            <div class="bg-inner"></div>
          </div>
        </div>
        <button class="btn block-cube block-cube-hover" type="button">
          <div class="bg-top">
            <div class="bg-inner"></div>
          </div>
          <div class="bg-right">
            <div class="bg-inner"></div>
          </div>
          <div class="bg">
            <div class="bg-inner"></div>
          </div>
          <div class="text" onClick={handleLogin}>
            login
          </div>
        </button>
        <div class="credits">
          <a href="https://codepen.io/marko-zub/" target="_blank">
            My other codepens
          </a>
        </div>
        <div>
          <Link to={"/register"}>dang ki?</Link>
        </div>
        <Loginwapper>
          <Link to={"/admin-product"}>
            <div>
              <h1>quen mat khau ?</h1>
            </div>
          </Link>
        </Loginwapper>
      </form>
      <div>
        <Bglogin />
      </div>
    </div>
  );
};

export default Login;
export const Loginwapper = styled.div`
  width: 100%;
  height: 100%;
  padding-top: 20px;
  padding: 12px 12px;
  position: relative;
  z-index: 50;
  border: none;
  &::after {
    content: "";
    width: 100%;
    height: 100%;
    position: absolute;
    background: #ccc;
    border-radius: 20px;
    z-index: -1;
    left: 0;
    top: 12px;
    right: 0;
    bottom: 0;
    animation: bgrin 3s infinite linear;
  }
  @keyframes bgr {
    0% {
      transform: translateY(10px) rotateX(20deg);
      border-radius: 20px;
    }
    50% {
    }
    100% {
      transform: translateY(20px) rotateX(-50deg);
    }
  }
  @keyframes bgrin {
    0% {
      width: 10%;
      opacity: 0;
    }
    50% {
    }
    100% {
      width: 100%;
      opacity: 0.5;
    }
  }
  h1 {
    color: red;
    animation: bgr 3.2s infinite linear;
    cursor: pointer;
    font-size: 18px;
    padding: 12px 12px;
  }
`;
