import { useParams } from "react-router-dom/cjs/react-router-dom";
import { instanceAuth } from "../../utils/axios";
import { useEffect, useState } from "react";
import "./detail.css";
const Detail = () => {
  const { id } = useParams();
  const [data, setData] = useState({});
  const handleGetDetail = async () => {
    await instanceAuth
      .get(`https://api-init-kvep.onrender.com/api/product/get-details/${id}`)
      .then((res) => {
        console.log(res);
        setData(res?.data?.data);
      });
  };
  useEffect(() => {
    handleGetDetail();
  }, []);
  return (
    <div className="app">
      <div className="details" key={data._id}>
        <div className="big-img">
          {data?.image?.map((img, index) => (
            <img key={index} src={img} alt="" />
          ))}
        </div>
        <div className="box">
          <div
            className="row"
            style={{
              display: "flex",
              alignItems: "center",
              textAlign: "center",
            }}
          >
            <h2>{data.name}</h2>
            <span>${data.price}</span>
          </div>
          <div className="row">
            <h2> rating {data.rating}</h2>
            <h2> discount {data.discount}</h2>
            <h2> type {data.type}</h2>
          </div>
          <div className="row">
            <h2> createdAt {data.createdAt}</h2>
          </div>
          <div className="colors"></div>

          <p>{data.description}</p>

          {/* <DetailsThumb images={data.src} tab={this.handleTab} myRef={this.myRef} /> */}
          <button className="cart">Add to cart</button>
        </div>
      </div>
    </div>
  );
};
export default Detail;

let colors = ["red", "black", "crimson", "teal"];
