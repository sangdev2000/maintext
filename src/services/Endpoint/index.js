const ENDPOINT = {
  GETALLPRODUCT: "api/product/get-all",
  LOGIN: "api/user/sign-in",
  CREATEPRODUCT: "api/product/create",
  GETALLUSER: "api/user/getAll",
  GETPRODUCTDUMY: "/products",
};
export default ENDPOINT;
