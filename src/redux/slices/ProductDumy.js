import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
// import { axiosInstanceDumy } from "../../utils/axios";
import ENDPOINT from "../../services/Endpoint";
// login api call
// const initialState = {
//   ProductListdumy: [],
//   loading: false,
// };
// export const getAllProductDumy = createAsyncThunk(
//   "get-All-Product-dumy",
//   async (_param, thunkAPI) => {
//     try {
//       const response = await axiosInstanceDumy.get(ENDPOINT.GETPRODUCTDUMY, {
//         signal: thunkAPI.signal,
//       });
//       console.log("dumyyy", response);
//       return response;
//     } catch (ex) {
//       return ex;
//     }
//   }
// );
// const ProductSlicedumy = createSlice({
//   name: "Productdumy",
//   initialState,
//   reducers: {},
//   extraReducers(builder) {
//     builder
//       .addCase(getAllProductDumy.fulfilled, (state, action) => {
//         state.ProductListdumy = action.payload;
//       })
//       .addMatcher(
//         (action) => action?.type.endsWith("/pending"),
//         (state, action) => {
//           state.loading = true;
//         }
//       )
//       .addMatcher(
//         (action) =>
//           action?.type.endsWith("/rejected") ||
//           action?.type.endsWith("/fulfilled"),
//         (state, action) => {
//           if (state.loading) {
//             state.loading = false;
//           }
//         }
//       );
//   },
// });
// const ProductReducerDumy = ProductSlicedumy.reducer;
// export default ProductReducerDumy;
