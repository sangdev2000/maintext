import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../utils/axios";
import ENDPOINT from "../../services/Endpoint";
// login api call
const initialState = {
  UserList: [],
  loading: false,
};
export const getAllProduct = createAsyncThunk(
  "get-All-User",
  async (_param, thunkAPI) => {
    try {
      const response = await axiosInstance.get(ENDPOINT.GETALLUSER, _param, {
        signal: thunkAPI.signal,
      });

      return response;
    } catch (ex) {
      return ex;
    }
  }
);
const listUserSlice = createSlice({
  name: "listuser",
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(getAllProduct.fulfilled, (state, action) => {
        state.UserList = action.payload;
      })
      .addMatcher(
        (action) => action?.type.endsWith("/pending"),
        (state, action) => {
          state.loading = true;
        }
      )
      .addMatcher(
        (action) =>
          action?.type.endsWith("/rejected") ||
          action?.type.endsWith("/fulfilled"),
        (state, action) => {
          if (state.loading) {
            state.loading = false;
          }
        }
      );
  },
});

const ListUserReducer = listUserSlice.reducer;

export default ListUserReducer;
