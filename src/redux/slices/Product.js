import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../utils/axios";
import ENDPOINT from "../../services/Endpoint";
// login api call
const initialState = {
  ProductList: [],
  loading: false,
};
export const getAllProduct = createAsyncThunk(
  "get-All-Product",
  async (_param, thunkAPI) => {
    try {
      const response = await axiosInstance.get(
        `${ENDPOINT.GETALLPRODUCT}?limit=${_param.limit || 10}&page=${
          _param.page || 1
        }&filter&sort`,
        {
          signal: thunkAPI.signal,
        }
      );
      return response;
    } catch (ex) {
      return ex;
    }
  }
);
const ProductSlice = createSlice({
  name: "product",
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(getAllProduct.fulfilled, (state, action) => {
        state.ProductList = action.payload;
      })
      .addMatcher(
        (action) => action?.type.endsWith("/pending"),
        (state, action) => {
          state.loading = true;
        }
      )
      .addMatcher(
        (action) =>
          action?.type.endsWith("/rejected") ||
          action?.type.endsWith("/fulfilled"),
        (state, action) => {
          if (state.loading) {
            state.loading = false;
          }
        }
      );
  },
});
const ProductReducer = ProductSlice.reducer;
export default ProductReducer;
