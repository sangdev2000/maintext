import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axiosInstance from "../../utils/axios";
import ENDPOINT from "../../services/Endpoint";
import { SetSession } from "../../utils/SetSession";

const initialState = {
  loginList: {},
  loading: false,
  currentRequestId: null,
  isAuthenticated: false,
};

export const _login = createAsyncThunk("_login", async (_login, thunkAPI) => {
  try {
    const response = await axiosInstance.post(ENDPOINT.LOGIN, _login, {
      signal: thunkAPI.signal,
    });
    const token = response?.Data?.Token;
    // console.log(">>>>>", token);
    SetSession(token);
    return response;
  } catch (ex) {
    return ex;
  }
});

export const _logout = createAsyncThunk(
  "xfunder/_logout",
  async (_token, thunkAPI) => {
    try {
      const response = await axiosInstance.post(ENDPOINT.LOGOUT, _token, {
        signal: thunkAPI.signal
      });
      return response;
    } catch (ex) {
      return ex;
    }
  }
);

const loginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    //
  },
  extraReducers(builder) {
    builder
      .addCase(_login.fulfilled, (state, action) => {
        state.loginList = action.payload;
        state.isAuthenticated = true;
      })
      .addCase(_logout.fulfilled, (state, action) => {
        state.isAuthenticated = false;
        const _remove = action.payload?.StatusCode;
        if (_remove === 200) {
          localStorage.removeItem("token");
        }
      })
      .addMatcher(
        (action) => action?.type.endsWith("/pending"),
        (state, action) => {
          state.loading = true;
          state.currentRequestId = action.meta.requestId;
        }
      )
      .addMatcher(
        (action) =>
          action?.type.endsWith("/rejected") ||
          action?.type.endsWith("/fulfilled"),
        (state, action) => {
          if (
            state.loading &&
            state.currentRequestId === action.meta.requestId
          ) {
            state.loading = false;
            state.currentRequestId = null;
          }
        }
      );
  },
});

const loginReducer = loginSlice.reducer;

export default loginReducer;
