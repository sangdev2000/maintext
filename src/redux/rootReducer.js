import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";
// slices
import loginReducer from "./slices/login";
import ProductReducer from "./slices/Product";
import ListUserReducer from "./slices/GetallUser";

// ----------------------------------------------------------------------

export const rootPersistConfig = {
  key: "root",
  storage,
  keyPrefix: "redux-",
  whitelist: [],
};
export const productPersistConfig = {
  key: "product",
  storage,
  keyPrefix: "redux-",
  whitelist: ["sortBy", "checkout"],
};
export const listuserPersistConfig = {
  key: "listuser",
  storage,
  keyPrefix: "redux-",
  whitelist: [],
};

const rootReducer = combineReducers({
  login: loginReducer,
  product: ProductReducer,
  listuser: ListUserReducer,
});

export default rootReducer;
